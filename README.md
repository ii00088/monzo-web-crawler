# Monzo-web-crawler

## Usage
```
$ make build
$ make run url=monzo.com #where monzo.com can be any domain
```

## Testing
```
$ make test
```

## Dependencies
- You'd need to have GOPATH exported.  GOBIN for MACOS for go get command