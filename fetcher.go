package main

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

//Function that fetches all the links on a given URL from a GET request
//checks if the document contains duplicates as well as if links are
//internal.
func fetchLinks(path string, base string) []string {
	res, _ := Request(path)
	doc, _ := goquery.NewDocumentFromResponse(res)
	links := extractLinks(doc)
	foundUrls := checkInternal(base, links)
	return foundUrls
}

//Function to perform the GET request on an URL and return the response.
func Request(url string) (*http.Response, error) {
	//Http Client initilisation
	client := &http.Client{}
	//Perform the request using Header to specify GoogleBot web-crawler
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("User-Agent", "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)")
	//Response and check errors.
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	return res, nil
}

//Function to extract all hyperlinks within a document returned from
//http.Response in the form of a map.
func extractLinks(doc *goquery.Document) map[string]bool {
	found := make(map[string]bool)
	if doc != nil {
		doc.Find("a").Each(func(i int, s *goquery.Selection) {
			res, _ := s.Attr("href")
			found[res] = true
		})
		return found
	}
	return found
}

//Function to check if the website is internal to the domain
//as outlined by the task.
func checkInternal(base string, links map[string]bool) []string {
	internalUrls := []string{}
	for link, _ := range links {
		if strings.HasPrefix(link, base) {
			internalUrls = append(internalUrls, link)
		} else if strings.HasPrefix(link, "/") {
			resolvedURL := fmt.Sprintf("%s%s", base, link)
			internalUrls = append(internalUrls, resolvedURL)
		} else {
			continue
		}
	}
	return internalUrls
}
