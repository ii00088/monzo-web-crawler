package main

import "net/url"

//Function to convert user input to valid url
func Domain(ui string) (string, error) {
	if ui[len(ui)-1:] == "/" {
		ui = ui[:len(ui)-1]
	}

	parse, err := url.Parse(ui)
	if err != nil {
		InfoLogger.Println("USAGE: ./web-crawler [http://monzo.com]")
		return "", err
	}
	parse.Scheme = "http"
	return parse.String(), nil
}
