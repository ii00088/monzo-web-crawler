package main

import (
	"log"
	"os"
	"sync"
	"time"
)

//Structure to represent the crawled URLs.
//Conatins map of all urls and Mutex for handling race condition.
type UsedURL struct {
	urls map[string]bool
	mux  sync.RWMutex
}

//Declaring all the variables needed
var (
	InfoLogger  *log.Logger
	ErrorLogger *log.Logger
	domain      string
	err         error
)

func main() {
	//Initilising the 2 loggers
	InfoLogger = log.New(os.Stdout, "INFO: ", 0)
	ErrorLogger = log.New(os.Stderr, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
	//Check if the usage of the script has been correct.
	if len(os.Args) != 2 {
		InfoLogger.Println("USAGE: ./web-crawler [http://monzo.com]")
		return
	}

	//Get UserInput
	arg := os.Args[1]
	//Convert to valid url
	domain, _ = Domain(arg)

	//Initialise the map for crawled links.
	crawled := UsedURL{urls: make(map[string]bool)}
	//Initialise the WaitGroup needed to achieve the recursive concurrency.
	wg := new(sync.WaitGroup)
	//Add counter and start the alogrithm in parallel.
	wg.Add(1)
	go crawl(domain, &crawled, wg)
	wg.Wait()
}

//Recursive function to crawl each url on a domain. Uses WaitGroup to notify when an iteration has finished.
//Locks the mux when performing a Read and Write operations to achieve atomic add. Another WaitGroup is
//present so that we print results in order. Link crawled -> all hyperlinks present. Base condition of the
//function would be when all links on the domain are in the "used" variable.
func crawl(path string, used *UsedURL, wg *sync.WaitGroup) {
	//Decrement the WaitGroup counter by one.
	defer wg.Done()
	//Introducing slight delay - Trade off for synchronising printing
	time.Sleep(10 * time.Millisecond)
	//Lock the mutual exclusion so that only one thread can write to the variable.
	//Eliminate the race condition.
	used.mux.Lock()
	//Check if url has been crawled. Return if it has and unlock the Mutex.
	if used.urls[path] == true {
		used.mux.Unlock()
		return
	}
	//Mark the webpage as crawled and Unlock the mux.
	used.urls[path] = true
	used.mux.Unlock()
	//WaitGroup for synchronising concurrent crawls.
	wgC := new(sync.WaitGroup)
	//Retrieve all the links in that crawled page.
	fetched := fetchLinks(path, domain)
	InfoLogger.Println("Crawled: " + path)
	for _, link := range fetched {
		wgC.Add(1)
		InfoLogger.Println("Found↳ " + link)
		go crawl(link, used, wgC)
	}
	wgC.Wait()
	return
}
